# Sources


## Odds in `R`
* https://jarrettmeyer.com/2019/07/23/odds-ratio-in-r
* https://statsthinking21.github.io/statsthinking21-R-site/comparing-means-in-r.html


## Composites of two distributions (mixtures)
* https://stats.stackexchange.com/questions/179213/mean-of-two-normal-distributions
* https://nicercode.github.io/guides/mcmc/
* https://www.burtonsys.com/climate/composite_standard_deviations.html


## Overlap of two normal distributions
* https://stats.stackexchange.com/questions/103800/calculate-probability-area-under-the-overlapping-area-of-two-normal-distributi
* https://rpsychologist.com/calculating-the-overlap-of-two-normal-distributions-using-monte-carlo-integration

## Normal Distribution Probabilities
* https://www.geeksforgeeks.org/create-matrix-and-data-frame-from-lists-in-r-programming/
