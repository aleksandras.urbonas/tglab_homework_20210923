# tglab_homework_20210923

## Name
Sports Betting Probability Analyzer

## Description
This project tackles a question of normal distribution and it's probability densities for sports betting.

## Visuals
N/A

## Installation
Install `R` and `Rstudio`. Proceed to ## Usage.

## Usage
Run /01_code/_main_.R file, it contains all links to sources.

## Support
Please get in touch by email: aleksandras.urbonas at gmail.com

## Roadmap
N/A

## Authors and acknowledgment
Aleksandras Urbonas

## License
Copyright Aleksandras Urbonas 2021

## Project status
Ongoing
